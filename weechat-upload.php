<?php
// weechat-upload-script (FLOSS) CEL 2022-2023
if (!isset($_SERVER['PHP_AUTH_USER'])) {
	header('WWW-Authenticate: Basic realm="weechat upload"');
	header('HTTP/1.0 401 Unauthorized');
	exit;
} else if ($_SERVER['PHP_AUTH_USER'] === 'EXAMPLE_USER' && $_SERVER['PHP_AUTH_PW'] === 'EXAMPLE_PASSWORD') {
} else {
	header('WWW-Authenticate: Basic realm="weechat upload"');
	header('HTTP/1.0 401 Unauthorized');
	exit;
}

// ini_set('display_errors', 1);
// ini_set('html_errors', 0);
// error_reporting(E_ALL);

$file = @$_FILES['file'];
if (!$file) exit;
$name = $file['name'];
$tmp_filename = $file['tmp_name'];
if (!is_uploaded_file($tmp_filename)) exit;
$base='https://EXAMPLE.ORG/';

if (preg_match('/^IMG_(20\d\d)(\d\d)(\d\d)_(.*)$/', $name, $m)) {
	$dir = $m[1].'/'.$m[2].'/'.$m[3].'/';
	$name = $m[4];
} else if (preg_match('/^Screenshot_(20\d\d)(\d\d)(\d\d)-(.*)$/', $name, $m)) {
	$dir = $m[1].'/'.$m[2].'/'.$m[3].'/';
	$name = $m[4];
} else {
	$dir = date('Y/m/d/');
}
@mkdir($dir, 0755, true);
@chdir($dir);

while (file_exists($name)) {
	if (!preg_match('/^(.*?)(?:\\.([1-9][0-9]*))?(\\.[^0-9]+?)$/', $name, $m)) {
		header($_SERVER['SERVER_PROTOCOL'].' 409 Conflict');
		@unlink($tmp_filename);
		die('Unable to pick filename');
	}
	$i = (int)$m[2] + 1;
	$prev_filename = $name;
	$name = $m[1].'.'.$i.$m[3];
	if ($name == $prev_filename) {
		header($_SERVER['SERVER_PROTOCOL'].' 409 Conflict');
		@unlink($tmp_filename);
		die('Unable to pick filename');
	}
}
if (!move_uploaded_file($tmp_filename, $name)) {
	@unlink($tmp_filename);
	die('Unable to save file.');
}
@chmod($name, 0644);
// error_log($name);
print $base.$dir.rawurlencode($name);
